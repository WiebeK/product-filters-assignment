# Product Filters
Product filters with NextJS, useSWR and styled modules based on [this assignment](https://www.notion.so/Assignment-filter-f876889cb1864697942a688bcc91a037).

## Getting started
1. Install dependencies with `yarn`
0. Run `yarn dev`