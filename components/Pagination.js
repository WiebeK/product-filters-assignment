import ReactJsPagination from "react-js-pagination";
import styles from "./Pagination.module.css";

const Pagination = ({onChange, page, total, limit}) => {
    return <section className={styles.container}>
        <ReactJsPagination
            onChange={onChange}
            activePage={page}
            totalItemsCount={total}
            itemsCountPerPage={limit}
            itemClass={styles.item}
            activeClass={styles.active}
        />
        <span>{total} products</span>
    </section>;
}

export default Pagination;