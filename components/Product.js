import styles from "./Product.module.css";

const Product = ({ product, className }) => {
    return <article className={className}>
        <img src={product.image} alt={product.name} className={styles.image}/>
        <p className={styles.name}>{product.name}</p>
        <span className={styles.price}>${product.displayPrice}</span>
    </article>
}

export default Product;