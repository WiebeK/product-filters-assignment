const ProductFilter = ({name, label, options, selectedOptions, onChange, multiple, className}) => {
    const value = multiple ? selectedOptions : selectedOptions[0] ?? '';

    const handleSelectedOptionsChange = (event) => {
        const values = [...event.target.selectedOptions].map(option => option.value);
        const singleValue = values[0] !== '' ? values : [];
        onChange(multiple ? values : singleValue);
    }

    return <section className={className}>
        <label htmlFor={name}>{label}</label>
        <select id={name} onChange={handleSelectedOptionsChange} value={value} multiple={multiple}>
            {!multiple && <option value=""/>}
            {options.map((option, index) =>
                <option key={index} value={option}>{option}</option>
            )}
        </select>
    </section>
}

export default ProductFilter;