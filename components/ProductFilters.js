import ProductFilter from "./ProductFilter";
import styles from './ProductFilters.module.css'

const ProductFilters = ({filters, selectedOptions, onFiltersChange}) => {
    const handleFilterChange = (name, values) => {
        const options = {...selectedOptions};
        options[name] = values.length ? values : undefined;
        onFiltersChange(options)
    }

    return <section className={styles.container}>
        {filters.map((filter, index) => <ProductFilter
            key={index}
            className={styles.filter}
            name={filter.name}
            label={filter.label}
            options={filter.options}
            selectedOptions={selectedOptions[filter.name] ?? []}
            onChange={values => handleFilterChange(filter.name, values)}
            multiple={filter?.multiple}
        />)}
    </section>;
}

export default ProductFilters;