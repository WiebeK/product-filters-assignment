import Product from "./Product";
import styles from './ProductGrid.module.css';

const ProductGrid = ({products}) => {
    return <section className={styles.grid}>
        {products.map((product) =>
            <Product key={product.name} product={product} className={styles.product}/>
        )}
    </section>;
}

export default ProductGrid;