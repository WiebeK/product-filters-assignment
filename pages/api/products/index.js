import ProductRepository from "../../../services/ProductService";

const ensureArray = (value) => Array.isArray(value) ? value : [value];

const productHasColors = (product, colors) => {
    return colors.every((color) => product.colorFamilies.indexOf(color) >= 0);
}

const productHasCategories = (product, categories) => {
    return categories.every((category) => product.categories.indexOf(category) >= 0);
}

const productIsInPriceRange = (product, priceRange) => {
    const prices = priceRange.match(/([0-9]+)-([0-9]+)/);

    if (!prices || prices.length !== 3) {
        return true;
    }

    const min = parseInt(prices[1]);
    const max = parseInt(prices[2]);

    const outOfRange = product.prices.some((price) => {
        return price < min || price > max;
    });

    return !outOfRange;
}

export default async (request, response) => {
    const {
        query: {
            colors,
            categories,
            priceRange,
            page: page = 1,
            limit: limit = 10
        }
    } = request;

    const products = ProductRepository.getProducts();

    const filteredProducts = products.filter((product) => {
        if (colors && !productHasColors(product, ensureArray(colors))) {
            return false;
        }

        if (categories && !productHasCategories(product, ensureArray(categories))) {
            return false;
        }

        if (priceRange && !productIsInPriceRange(product, priceRange))
            return false;

        return true;
    });

    const pageStart = (page - 1) * limit;
    const pageEnd = page * limit;
    const paginatedProducts = filteredProducts.slice(pageStart, pageEnd);

    response.status(200).json({
        meta: {
            page: +page,
            limit: +limit,
            total: filteredProducts.length
        },
        products: paginatedProducts
    });
}