import Head from 'next/head'
import {useRouter} from "next/router";
import useSWR from "swr";
import ProductGrid from "../components/ProductGrid";
import ProductFilters from "../components/ProductFilters";
import Pagination from "../components/Pagination";
import QueryService from "../services/QueryService";
import styles from '../styles/Home.module.css'
import ProductService from "../services/ProductService";

const productsFetcher = async (queryString) => {
    const response = await fetch(`http://localhost:3000/api/products?${queryString}`);
    return await response.json();
}

export default function Home({filters, initialProductsResponse}) {
    const router = useRouter();

    const filterParams = QueryService.extractParams(router.query, ['categories', 'colors', 'priceRange']);
    const selectedFilterOptions = QueryService.ensureArrays(filterParams);

    const paginationParams = QueryService.extractParams(router.query, ['page', 'limit']);
    const productsParams = QueryService.sortKeys({...filterParams, ...paginationParams});

    const productsQueryString = new URLSearchParams(productsParams).toString();

    const swrConfig = {initialData: initialProductsResponse};
    const {data: productsResponse} = useSWR(productsQueryString, productsFetcher, swrConfig);

    const products = productsResponse.products;
    const pagination = productsResponse.meta;

    const pushQuery = (query) => {
        const cleanQuery = QueryService.removeUndefinedParams(query);

        if (!Object.keys(cleanQuery).length) {
            router.push(router.pathname, undefined, {shallow: true});
        } else {
            router.push({query: cleanQuery}, undefined, {shallow: true})
        }
    };

    const handlePageChange = (page) => {
        const query = {...router.query, page};
        pushQuery(query);
    }

    const handleFiltersChange = (filters) => {
        const query = {...router.query, ...filters, page: 1}
        pushQuery(query);
    }

    return (
        <>
            <Head>
                <title>Products</title>
            </Head>
            <main className={styles.main}>
                <h1 className={styles.title}>Products</h1>
                <section className={styles.filters}>
                    <h2>Filters</h2>
                    <ProductFilters
                        selectedOptions={selectedFilterOptions}
                        onFiltersChange={handleFiltersChange}
                        filters={filters}
                    />
                </section>
                {products.length ?
                    <>
                        <ProductGrid products={products}/>
                        <Pagination
                            page={pagination.page}
                            limit={pagination.limit}
                            total={pagination.total}
                            onChange={handlePageChange}
                        />
                    </> : <section className={styles.noProducts}><p>No products found.</p></section>}
            </main>
        </>
    )
}

export const getServerSideProps = async ({query}) => {
    const filters = ProductService.getFilters();
    const initialProductsResponse = await productsFetcher(new URLSearchParams(query).toString());
    return {
        props: {
            filters, initialProductsResponse
        }
    }
}