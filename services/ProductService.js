import products from '../data/products.json';

const getColorFamilies = () => {
    const colorFamilies = new Set();

    for (const product of products) {
        const productColorFamilies = product.node.colorFamily?.map((colorFamily) => colorFamily.name) ?? [];
        colorFamilies.add(...productColorFamilies);
    }

    colorFamilies.delete(undefined);

    return [...colorFamilies].sort();
}

const getCategories = () => {
    const categories = new Set();

    for (const product of products) {
        const productCategories = product.node.categoryTags ?? [];
        categories.add(...productCategories);
    }

    categories.delete(undefined);

    return [...categories].sort();
}

const getProductPrices = (product) => {
    const variants = product.node.shopifyProductEu?.variants?.edges ?? [];
    return variants.map(variant => variant.node.price);
}

const getPriceRanges = (size = 100) => {
    const pricesPerProduct = products.map(product => getProductPrices(product));
    const allPrices = [].concat(...pricesPerProduct);
    const maxPrice = new Float64Array(allPrices).sort().slice(-1)[0] ?? 0;

    const priceRanges = []

    if (maxPrice > size) {
        priceRanges.push([`0-${size}`]);

        for (let i = 1; i < (maxPrice / size); i++) {
            const min = size * i;
            const calculatedMax = min + size;
            const max = calculatedMax > maxPrice ? maxPrice : calculatedMax;
            priceRanges.push(`${min}-${max}`);
        }
    } else {
        priceRanges.push([`0-${maxPrice}`]);
    }

    return priceRanges;
}

const getProducts = () => {
    return products.map((product) => {
        const name = product.node.name;
        const image = product.node.thumbnailImage.file.url;
        const prices = getProductPrices(product);
        const displayPrice = (prices.length ? Math.max(...prices) : 0).toFixed(2);
        const colorFamilies = product.node.colorFamily?.map((colorFamily) => colorFamily.name) ?? [];
        const categories = product.node.categoryTags ?? [];

        return {name, image, prices, displayPrice, colorFamilies, categories}
    })
};

const getFilters = () => {
    return [
        {
            name: 'categories',
            label: 'Categories',
            options: getCategories(),
            multiple: true
        },
        {
            name: 'colors',
            label: 'Colors',
            options: getColorFamilies(),
            multiple: true
        },
        {
            name: 'priceRange',
            label: 'Price range',
            options: getPriceRanges()
        }
    ]
}

export default {getProducts, getFilters};