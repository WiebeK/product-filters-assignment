const removeUndefinedParams = (query) => {
    const queryClone = Object.assign({}, query);

    Object.keys(queryClone).forEach((key) => {
        if (query[key] === undefined) {
            delete queryClone[key];
        }
    });

    return queryClone;
}

const extractParams = (query, keys) => {
    const queryClone = Object.assign({}, query);

    Object.keys(queryClone).forEach(key => {
        if (keys.indexOf(key) === -1) {
            delete queryClone[key];
        }
    });

    return queryClone;
}

const ensureArrays = (query) => {
    const queryClone = Object.assign({}, query);

    Object.keys(queryClone).forEach(key => {
        if (!Array.isArray(queryClone[key])) {
            queryClone[key] = [queryClone[key]];
        }
    });

    return queryClone;
}

const sortKeys = (query) => {
    return Object.keys(query).sort().reduce(function (sortedQuery, key) {
        sortedQuery[key] = query[key];
        return sortedQuery;
    }, {});
}

export default {
    removeUndefinedParams,
    extractParams,
    ensureArrays,
    sortKeys
}